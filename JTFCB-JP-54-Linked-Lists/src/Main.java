import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		// ArrayList manage arrays internally
		
		// trzeba myslec jaka stworzyc instancje listy czyli te 2 linijki ponizej, pozniej sie juz nimi nie przejmujesz
		// kiedy chcesz usuwac zawartosc z konca to uzywaj ArrayList
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		
		
		// kiedy chcesz usuwac ze srodka lub z poczatku to uzywaj LinkedList
		// LinkedList consists of elements where each element has a 
		// reference to the previous and next element
		LinkedList<Integer>linkedList = new LinkedList<Integer>();
		
		doTimings("ArrayList", arrayList);
		doTimings("LinkedList", linkedList);
	}

	private static void doTimings(String type, List<Integer> list) {
		
		for(int i = 0; i < 1E5; i++) {
			
			list.add(i);
			
		}
		long start = System.currentTimeMillis();
		
		// add items at the end of list
//		for(int i =0; i < 1E5; i++) {
//			list.add(i);
//		}
		
		// Add items elswhere in list
		for(int i=0; i <1E5; i++) {
			list.add(0,i);
			
		}
		
		long end = System.currentTimeMillis();
		
		System.out.println("Time taken: " + (end - start) + "ms for" + type);
	}
	
	
}
